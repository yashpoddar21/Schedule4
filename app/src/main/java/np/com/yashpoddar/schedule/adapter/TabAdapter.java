package np.com.yashpoddar.schedule.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import np.com.yashpoddar.schedule.fragment.offlinefragment.FridayFragment;
import np.com.yashpoddar.schedule.fragment.offlinefragment.MondayFragment;
import np.com.yashpoddar.schedule.fragment.offlinefragment.SaturdayFragment;
import np.com.yashpoddar.schedule.fragment.offlinefragment.SundayFragment;
import np.com.yashpoddar.schedule.fragment.offlinefragment.ThursdayFragment;
import np.com.yashpoddar.schedule.fragment.offlinefragment.TuesdayFragment;
import np.com.yashpoddar.schedule.fragment.offlinefragment.WednesdayFragment;

public class TabAdapter extends FragmentStatePagerAdapter {

    public TabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new SundayFragment();
        } else if (position == 1) {
            return new MondayFragment();
        } else if (position == 2) {
            return new TuesdayFragment();
        } else if (position == 3) {
            return new WednesdayFragment();
        } else if (position == 4) {
            return new ThursdayFragment();
        } else if (position == 5) {
            return new FridayFragment();
        } else if (position == 6) {
            return new SaturdayFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 7;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return "Sunday";
        } else if (position == 1) {
            return "Monday";
        } else if (position == 2) {
            return "Tuesday";
        } else if (position == 3) {
            return "Wednesday";
        } else if (position == 4) {
            return "Thursday";
        } else if (position == 5) {
            return "Friday";
        } else if (position == 6) {
            return "Saturday";
        }
        return null;
    }
}