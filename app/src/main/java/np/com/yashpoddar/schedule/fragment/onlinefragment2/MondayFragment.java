package np.com.yashpoddar.schedule.fragment.onlinefragment2;


import android.content.ContentValues;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Iterator;

import np.com.yashpoddar.schedule.R;
import np.com.yashpoddar.schedule.adapter.ScheduleAdapter;
import np.com.yashpoddar.schedule.database.DatabaseHandler;
import np.com.yashpoddar.schedule.model.SubjectEntity;

public class MondayFragment extends Fragment {

    private ListView listView;
    private DatabaseReference databaseReference;
    private ArrayList<SubjectEntity> list;
    private ScheduleAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_online, container, false);

        listView = (ListView) v.findViewById(R.id.listView);

        list = new ArrayList<>();

        adapter = new ScheduleAdapter(getContext(), list);

        listView.setAdapter(adapter);

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Monday1");
        databaseReference.keepSynced(true);
        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                list.add(new SubjectEntity(StringUtils.substringAfter(dataSnapshot.getKey(), "="), makeArray(dataSnapshot.getValue().toString())));
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return v;
    }

    public ArrayList<String> makeArray(String string) {
        ArrayList<String> listString = new ArrayList<>();
        if(string.contains(",")) {
            String str[] = StringUtils.substringsBetween(string, "=", ",");
            for (String str1 : str) {
                listString.add(str1);
            }
        }else{
            String str = StringUtils.substringAfter(string, "=");
            str = str.replace("}","");
            listString.add(str);
        }
        return listString;
    }
}
