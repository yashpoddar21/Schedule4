package np.com.yashpoddar.schedule.activity;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import np.com.yashpoddar.schedule.R;
import np.com.yashpoddar.schedule.database.DatabaseHandler;
import np.com.yashpoddar.schedule.model.Schedule;

public class HomePage extends AppCompatActivity {
    static EditText stime;
    static EditText etime;
    EditText activity;
    Spinner day;
    DatabaseHandler databaseHandler;
    TimePickerFragment fragment1;
    TimePickerFragment1 fragment2;
    static String selectedStartTime, selectedEndTime;
    Button btn, btn1;

    Schedule schedule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        schedule = (Schedule) getIntent().getSerializableExtra("schedule");
        btn = (Button) findViewById(R.id.button);
        btn1 = (Button) findViewById(R.id.button1);
        btn1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });


        stime = (EditText) findViewById(R.id.startTime);
        stime.setKeyListener(null);
        etime = (EditText) findViewById(R.id.endTime);
        etime.setKeyListener(null);
        activity = (EditText) findViewById(R.id.activity);
        day = (Spinner) findViewById(R.id.spinner_day);
        databaseHandler = new DatabaseHandler(this);
        fragment1 = new TimePickerFragment();
        fragment2 = new TimePickerFragment1();

        if (schedule != null) {
            btn.setText("Edit");

            String stimeArray[] = schedule.getsTime().split(":");
            int stimeArrHr = Integer.parseInt(stimeArray[0]);
            int stimArrMin = Integer.parseInt(stimeArray[1]);


            System.out.println(stimeArrHr);

            String min = stimArrMin > 9 ? stimArrMin + "" : "0" + stimArrMin;

            if (stimeArrHr > 12) {
                stime.setText((stimeArrHr - 12) + ":" + min + " PM");
            } else if (stimeArrHr == 0) {
                stime.setText(12 + ":" + min + " AM");
            } else if (stimeArrHr == 12) {
                stime.setText(12 + ":" + min + " PM");
            } else {
                stime.setText(stimeArrHr + ":" + min + " AM");
            }


            String etimeArray[] = schedule.geteTime().split(":");
            int etimeArrHr = Integer.parseInt(etimeArray[0]);
            int etimArrMin = Integer.parseInt(etimeArray[1]);


            String min1 = etimArrMin > 9 ? etimArrMin + "" : "0" + etimArrMin;

            if (etimeArrHr > 12) {
                etime.setText((etimeArrHr - 12) + ":" + min1 + " PM");
            } else if (etimeArrHr == 0) {
                etime.setText(12 + ":" + min1 + " AM");
            } else if (etimeArrHr == 12) {
                etime.setText(12 + ":" + min1 + " PM");
            } else {
                etime.setText(etimeArrHr + ":" + min1 + " AM");
            }
            activity.setText(schedule.getActivity());

            selectedStartTime = schedule.getsTime();
            selectedEndTime = schedule.geteTime();

        }

        getSupportActionBar().setDisplayShowHomeEnabled(true);

        stime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment fragment = new TimePickerFragment();
                fragment.show(getFragmentManager(), "time1");
            }
        });

        etime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerFragment1().show(getFragmentManager(), "stime");
            }
        });


    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);
            return new TimePickerDialog(getActivity(), this, hour, minute, false);
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {


            String min = minute > 9 ? minute + "" : "0" + minute;
            selectedStartTime = hourOfDay + ":" + min;


            if (hourOfDay > 12) {
                stime.setText((hourOfDay - 12) + ":" + min + " PM");
            } else if (hourOfDay == 0) {
                stime.setText(12 + ":" + min + " AM");
            } else if (hourOfDay == 12) {
                stime.setText(12 + ":" + min + " PM");
            } else {
                stime.setText(hourOfDay + ":" + min + " AM");
            }

        }
    }

    public static class TimePickerFragment1 extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);
            return new TimePickerDialog(getActivity(), this, hour, minute, false);
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            String min = minute > 9 ? minute + "" : "0" + minute;

            selectedEndTime = hourOfDay + ":" + min;

            if (hourOfDay > 12) {
                etime.setText((hourOfDay - 12) + ":" + min + " PM");
            } else if (hourOfDay == 0) {
                etime.setText(12 + ":" + min + " AM");
            } else if (hourOfDay == 12) {
                etime.setText(12 + ":" + min + " PM");
            } else {
                etime.setText(hourOfDay + ":" + min + " AM");
            }
        }
    }

    public int dayToNumber(String day) {

        String d = day.toLowerCase();
        switch (d) {
            case "sunday":
                return 1;
            case "monday":
                return 2;
            case "tuesday":
                return 3;
            case "wednesday":
                return 4;
            case "thursday":
                return 5;
            case "friday":
                return 6;
            case "saturday":
                return 7;
            default:
                return 1;

        }
    }

    public void addSchedule(View view) {

        stime.setError(null);
        etime.setError(null);

        if (TextUtils.isEmpty(stime.getText().toString())) {
            stime.setError("Your starting time cannot be empty");
            return;
        }

        if (TextUtils.isEmpty(etime.getText().toString())) {
            etime.setError("Your ending time cannot be empty");
            return;
        }

        int s1 = Integer.parseInt(selectedStartTime.replace(":", ""));
        int s2 = Integer.parseInt(selectedEndTime.replace(":", ""));

        if (s1 == s2) {
            Toast.makeText(HomePage.this, "Starting time and ending time cannot be the same", Toast.LENGTH_SHORT).show();
            return;
        }

        if (s1 > s2) {
            Toast.makeText(HomePage.this, "Starting time cannot be greater than the ending time", Toast.LENGTH_SHORT).show();
            return;
        }

        ContentValues contentValues = new ContentValues();

        contentValues.put("activity", activity.getText().toString());
        contentValues.put("stime", selectedStartTime);
        contentValues.put("entime", selectedEndTime);
        contentValues.put("day", dayToNumber(day.getSelectedItem().toString()));

        if (btn.getText().toString().equalsIgnoreCase("Edit")) {
            databaseHandler.update(contentValues, schedule.get_id() + "");
        } else {
            databaseHandler.addRecord(contentValues);
        }

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();


    }

    public void goBack(View view) {
        finish();
    }
}
