package np.com.yashpoddar.schedule.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import np.com.yashpoddar.schedule.fragment.onlinefragment2.FridayFragment;
import np.com.yashpoddar.schedule.fragment.onlinefragment2.MondayFragment;
import np.com.yashpoddar.schedule.fragment.onlinefragment2.ThursdayFragment;
import np.com.yashpoddar.schedule.fragment.onlinefragment2.TuesdayFragment;
import np.com.yashpoddar.schedule.fragment.onlinefragment2.WednesdayFragment;


public class OnlineTabAdapter2 extends FragmentStatePagerAdapter {

    public OnlineTabAdapter2(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new MondayFragment();
        } else if (position == 1) {
            return new TuesdayFragment();
        } else if (position == 2) {
            return new WednesdayFragment();
        } else if (position == 3) {
            return new ThursdayFragment();
        } else if (position == 4) {
            return new FridayFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return "Monday";
        } else if (position == 1) {
            return "Tuesday";
        } else if (position == 2) {
            return "Wednesday";
        } else if (position == 3) {
            return "Thursday";
        } else if (position == 4) {
            return "Friday";
        }
        return null;
    }
}