package np.com.yashpoddar.schedule.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import java.util.Calendar;

import np.com.yashpoddar.schedule.R;
import np.com.yashpoddar.schedule.database.DatabaseHandler;
import np.com.yashpoddar.schedule.fragment.offlinefragment.FridayFragment;
import np.com.yashpoddar.schedule.fragment.offlinefragment.MondayFragment;
import np.com.yashpoddar.schedule.fragment.offlinefragment.SaturdayFragment;
import np.com.yashpoddar.schedule.fragment.offlinefragment.SundayFragment;
import np.com.yashpoddar.schedule.fragment.offlinefragment.ThursdayFragment;
import np.com.yashpoddar.schedule.fragment.offlinefragment.TuesdayFragment;
import np.com.yashpoddar.schedule.fragment.offlinefragment.WednesdayFragment;

public class MainActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new MyAdapter(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);

        Calendar calendar = Calendar.getInstance();

        viewPager.setCurrentItem(calendar.get(Calendar.DAY_OF_WEEK) - 1);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HomePage.class);
                startActivity(intent);
            }
        });

    }

    private class MyAdapter extends FragmentStatePagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return new SundayFragment();
            } else if (position == 1) {
                return new MondayFragment();
            } else if (position == 2) {
                return new TuesdayFragment();
            } else if (position == 3) {
                return new WednesdayFragment();
            } else if (position == 4) {
                return new ThursdayFragment();
            } else if (position == 5) {
                return new FridayFragment();
            } else if (position == 6) {
                return new SaturdayFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 7;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return "Sunday";
            } else if (position == 1) {
                return "Monday";
            } else if (position == 2) {
                return "Tuesday";
            } else if (position == 3) {
                return "Wednesday";
            } else if (position == 4) {
                return "Thursday";
            } else if (position == 5) {
                return "Friday";
            } else if (position == 6) {
                return "Saturday";
            }
            return null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);


        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.deleteall:
                new AlertDialog.Builder(this)
                        .setMessage("Are you sure you want to delete all your data?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                new DatabaseHandler(MainActivity.this).empty();

                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                finish();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
