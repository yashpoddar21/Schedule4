package np.com.yashpoddar.schedule.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by yashpoddar on 8/25/16.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static int dbversion = 1;
    private static String db_name = "data";
    String sql, table_data;

    public DatabaseHandler(Context context) {
        super(context, db_name, null, dbversion);
        sql = "create table schedule(" +
                "_id integer  primary key autoincrement," +
                "activity text,stime text,entime text, day integer" +
                ")";
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table schedule if exists");
        dbversion++;
    }

    public boolean addRecord(ContentValues values) {
        SQLiteDatabase db = getWritableDatabase();
        return db.insert("schedule", null, values) > 0;
    }

    public Cursor getDataByDay(String day) {
        SQLiteDatabase db = getReadableDatabase();
        return db.query("schedule", null, "day=?", new String[]{day}, null, null, null);
    }

    public void empty() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("delete from  schedule");
    }

    public void deleteRecord(String id) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete("schedule", "_id=?", new String[]{id});
    }

    public void update(ContentValues values, String id) {
        SQLiteDatabase db = getWritableDatabase();
        db.update("schedule", values, "_id=?", new String[]{id});
    }
}
