package np.com.yashpoddar.schedule.model;

import java.io.Serializable;

/**
 * Created by yashpoddar on 8/13/16.
 */
public class Schedule implements Comparable<Schedule> ,Serializable{

    String sTime;
    String eTime;
    String activity;
    int day;
    int _id;

    private static long serialVersionUID=832737982938923L;

    public Schedule(){

    }

    public String getsTime() {
        return sTime;
    }

    public void setsTime(String sTime) {
        this.sTime = sTime;
    }

    public String geteTime() {
        return eTime;
    }

    public void seteTime(String eTime) {
        this.eTime = eTime;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    @Override
    public int compareTo(Schedule another) {

        String stimeOne= sTime.replace(":","");
        String stimeTwo= another.sTime.replace(":","");

        int stOne= Integer.parseInt(stimeOne);
        int stwo= Integer.parseInt(stimeTwo);
        return stOne-stwo;
    }
}
