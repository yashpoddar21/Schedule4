
package np.com.yashpoddar.schedule.fragment.offlinefragment;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import np.com.yashpoddar.schedule.database.DatabaseHandler;
import np.com.yashpoddar.schedule.activity.HomePage;
import np.com.yashpoddar.schedule.R;
import np.com.yashpoddar.schedule.model.Schedule;


/**
 * A simple {@link Fragment} subclass.
 */
public class ThursdayFragment extends Fragment {
    RecyclerView recyclerView ;

    DatabaseHandler databaseHandler;

    List<Schedule> data;

    MyAdapter adapter;

    public ThursdayFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_thursday, container, false);
        databaseHandler= new DatabaseHandler(getContext());
        data= new ArrayList<>();

        Cursor cursor = databaseHandler.getDataByDay("6");
        while (cursor.moveToNext()){
            Schedule schedule = new Schedule();
            schedule.set_id(cursor.getInt(0));
            schedule.setActivity(cursor.getString(1));
            schedule.setsTime(cursor.getString(2));
            schedule.seteTime(cursor.getString(3));
            schedule.setDay(cursor.getInt(4));
            data.add(schedule);
        }

        Collections.sort(data);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        adapter= new MyAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        return view;
    }

    class  MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(getContext()).inflate(R.layout.shedule_row,parent,false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {

            String stimes[]= data.get(position).getsTime().split(":");
            int stimeHour= Integer.parseInt(stimes[0]);
            int stimeMin= Integer.parseInt(stimes[1]);
            String stime="";

            if(stimeHour>12){
                stime+=(stimeHour-12)+":";
            }

            else if(stimeHour==0){
                stime+=12+":";
            }
            else {
                stime+=stimeHour+":";
            }


            if(stimeMin<10){
                stime+="0"+stimeMin;
            }else {
                stime+=stimeMin;
            }
            if(stimeHour>12){
                stime+=" PM";
            }
            else if(stimeHour==0){
                stime+=" AM";
            }
            else if(stimeHour==12){
                stime+=" PM";
            }
            else {
                stime+=" AM";
            }


            String etimes[]= data.get(position).geteTime().split(":");
            int etimeHour= Integer.parseInt(etimes[0]);
            int etimeMin= Integer.parseInt(etimes[1]);
            String etime="";





            if(etimeHour>12){
                etime+=(etimeHour-12)+":";
            }

            else if(etimeHour==0){
                etime+=12+":";
            }
            else {
                etime+=etimeHour+":";
            }

            if(etimeMin<10){
                etime+="0"+etimeMin;
            }else {
                etime+=etimeMin;
            }


            if(etimeHour>12){
                etime+=" PM";
            }
            else if(etimeHour==0){
                etime+=" AM";
            }
            else if(etimeHour==12){
                etime+=" PM";
            }
            else {
                etime+=" AM";
            }

            holder.tfTime.setText(stime+"-"+etime);
            holder.tfactivity.setText(data.get(position).getActivity());
            holder.postion=position;

            holder.view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    PopupMenu popup= new PopupMenu(getContext(),holder.tfTime);
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            System.out.println("clicked");
                            Schedule schedule= data.get(holder.getAdapterPosition());
                            switch (item.getItemId()){
                                case R.id.del:

                                    data.remove(holder.getAdapterPosition());
                                    adapter.notifyItemRemoved(holder.getAdapterPosition());
                                    databaseHandler.deleteRecord(schedule.get_id()+"");
                                    break;

                                case R.id.edit:
                                    Intent intent = new Intent(getContext(),HomePage.class);
                                    intent.putExtra("schedule",schedule);
                                    startActivity(intent);

                            }
                            return true;
                        }
                    });
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.action_menu, popup.getMenu());
                    popup.show();
                    return true;
                }
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder  {
            View view;
            TextView tfTime;
            TextView tfactivity;
            int postion;

            public MyViewHolder(View itemView) {
                super(itemView);

                tfactivity = (TextView) itemView.findViewById(R.id.tf_activity);
                tfTime = (TextView) itemView.findViewById(R.id.tf_time);
                view= itemView.findViewById(R.id.row);


            }


        }
    }

}
