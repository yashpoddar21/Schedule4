package np.com.yashpoddar.schedule.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import np.com.yashpoddar.schedule.fragment.onlinefragment.*;


public class OnlineTabAdapter extends FragmentStatePagerAdapter {

    public OnlineTabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new MondayFragment1();
        } else if (position == 1) {
            return new TuesdayFragment1();
        } else if (position == 2) {
            return new WednesdayFragment1();
        } else if (position == 3) {
            return new ThursdayFragment1();
        } else if (position == 4) {
            return new FridayFragment1();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return "Monday";
        } else if (position == 1) {
            return "Tuesday";
        } else if (position == 2) {
            return "Wednesday";
        } else if (position == 3) {
            return "Thursday";
        } else if (position == 4) {
            return "Friday";
        }
        return null;
    }
}