package np.com.yashpoddar.schedule.model;

import java.util.ArrayList;

/**
 * Created by sailesh on 6/18/17.
 */

public class SubjectEntity {
    public String time;
    public ArrayList<String> list;

    public SubjectEntity(String time, ArrayList<String> list) {
        this.time = time;
        this.list = list;
    }
}
