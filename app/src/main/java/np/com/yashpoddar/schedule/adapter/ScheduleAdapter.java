package np.com.yashpoddar.schedule.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import np.com.yashpoddar.schedule.R;
import np.com.yashpoddar.schedule.model.SubjectEntity;

/**
 * Created by sailesh on 6/18/17.
 */

public class ScheduleAdapter extends BaseAdapter {

    private ArrayList<SubjectEntity> list;
    private Context context;

    public ScheduleAdapter(Context context, ArrayList<SubjectEntity> list){
        this.context = context;
        this.list = list;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View v, ViewGroup parent) {
        v = null;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        v = inflater.inflate(R.layout.item_schedule, null);
        TextView textTime = (TextView) v.findViewById(R.id.txtTime);
        ListView listView = (ListView) v.findViewById(R.id.listView);

        textTime.setText(list.get(position).time);

        SubjectAdapter adapter = new SubjectAdapter(context, list.get(position).list);

        listView.setAdapter(adapter);
        setListViewHeightBasedOnChildren(listView);

        return v;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = (totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1)))+(listAdapter.getCount()*3);
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
