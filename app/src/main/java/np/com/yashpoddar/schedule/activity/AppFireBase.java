package np.com.yashpoddar.schedule.activity;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by sailesh on 6/21/17.
 */

public class AppFireBase extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
