package np.com.yashpoddar.schedule.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import np.com.yashpoddar.schedule.R;
import np.com.yashpoddar.schedule.database.DatabaseHandler;

public class FrontPageActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn1, btn2, btn3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_front_page);

        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn1) {
            Intent intent = new Intent(this, OnlineScheduleActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.btn2) {
            Intent intent = new Intent(this, OnlineScheduleActivity2.class);
            startActivity(intent);
        } else if (v.getId() == R.id.btn3) {
            Intent intent = new Intent(this, HomePage.class);
            startActivity(intent);
        }
    }
}
