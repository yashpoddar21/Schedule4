package np.com.yashpoddar.schedule.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import np.com.yashpoddar.schedule.R;

/**
 * Created by sailesh on 6/18/17.
 */

public class SubjectAdapter extends BaseAdapter {
    private ArrayList<String> listSubject;
    private Context context;

    public SubjectAdapter(Context context, ArrayList<String> listSubject){
        this.context = context;
        this.listSubject = listSubject;
    }
    @Override
    public int getCount() {
        return listSubject.size();
    }

    @Override
    public Object getItem(int position) {
        return listSubject.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = null;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        view = inflater.inflate(R.layout.item_subject, null);

        TextView txtSubject = (TextView) view.findViewById(R.id.txtSubject);
        txtSubject.setText(listSubject.get(position));
        return view;
    }
}
